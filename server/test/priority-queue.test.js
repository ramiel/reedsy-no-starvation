import test from 'ava'
import PriorityQueue from '../lib/priority-queue'

test('PriorityQueue can be instantiated', t => {
  t.notThrows(() => new PriorityQueue())
  t.truthy(new PriorityQueue())
})

test('PriorityQueue can be instantiated with a strategy', t => {
  const strategy = (a, b) => a + b
  const pq = new PriorityQueue(strategy)
  t.deepEqual(strategy, pq.strategy)
})

test('PriorityQueue has an push method', t => {
  const pq = new PriorityQueue()
  t.truthy(pq.push)
  t.true(pq.push instanceof Function)
})

test('PriorityQueue has a pop method', t => {
  const pq = new PriorityQueue()
  t.truthy(pq.pop)
  t.true(pq.pop instanceof Function)
})

test('PriorityQueue has a peek method', t => {
  const pq = new PriorityQueue()
  t.truthy(pq.peek)
  t.true(pq.peek instanceof Function)
})

test('PriorityQueue has a length method', t => {
  const pq = new PriorityQueue()
  t.truthy(pq.length)
  t.true(pq.length instanceof Function)
})

test('PriorityQueue has a isEmpty method', t => {
  const pq = new PriorityQueue()
  t.truthy(pq.isEmpty)
  t.true(pq.isEmpty instanceof Function)
})

test('isEmpty returns true if the queue is empty', t => {
  const pq = new PriorityQueue()
  t.true(pq.isEmpty())
})

test('isEmpty returns true if the last element is popped out', t => {
  const pq = new PriorityQueue()
  pq.push({priority: 0, value: 'A'})
  pq.pop()
  t.true(pq.isEmpty())
})

test('isEmpty returns false if the queue contains an element at least', t => {
  const pq = new PriorityQueue()
  pq.push({priority: 1, value: 'A'})
  t.false(pq.isEmpty())
})

test('Peek method returns a copy of the element', t => {
  const pq = new PriorityQueue()
  pq.push({priority: 1, value: 'A'})
  const el = pq.peek()
  el.value = 'B'
  t.is(pq.peek().value, 'A')
})

test('Peek give the head of the queue without parameters', t => {
  const pq = new PriorityQueue()
  const el = {priority: 1, value: 'A'}
  pq.push(el)
  t.deepEqual(el, pq.peek())
})

test('Peek method take an index as parameter', t => {
  const pq = new PriorityQueue()
  pq
    .push({priority: 1, value: 'A'})
    .push({priority: 1, value: 'B'})
  const el = pq.peek(1)
  t.is(el.value, 'B')
})

test('Pop remove the head of the queue', t => {
  const pq = new PriorityQueue()
  pq.push({priority: 1, value: 'A'})
  const el = pq.pop()
  t.is(el.value, 'A')
  t.is(pq.length(), 0)
})

test('Pop gives undefined if the queue is empty', t => {
  const pq = new PriorityQueue()
  const el = pq.pop()
  t.is(el, undefined)
})

test('Element are inserted following priority', t => {
  const pq = new PriorityQueue()
  const first = {priority: 1, value: 'B'}
  const second = {priority: 3, value: 'A'}
  pq
    .push(second)
    .push(first)
  t.deepEqual(pq.peek(), first)
})

test('Element with the same priority follow the insertion order', t => {
  const pq = new PriorityQueue()
  const samePriorityElements = [{priority: 1, value: 'A'}, {priority: 1, value: 'B'}, {priority: 1, value: 'C'}]
  samePriorityElements.forEach(el => pq.push(el))
  t.deepEqual(pq.peek(0).value, 'A')
  t.deepEqual(pq.peek(1).value, 'B')
  t.deepEqual(pq.peek(2).value, 'C')
})

test('Elements gain priority when surpassed', t => {
  const pq = new PriorityQueue()
  const first = {priority: 1, value: 'B'}
  const second = {priority: 3, value: 'A'}
  pq
    .push(second)
    .push(first)
  t.deepEqual(pq.peek(1), second)
  t.is(pq.peek(1).priority, 2)
})

test('Only a defined number of elements can surpass the one with less priority', t => {
  const pq = new PriorityQueue()
  const morePriorityElements = [{priority: 1, value: 'A'}, {priority: 1, value: 'B'}, {priority: 1, value: 'C'}]
  const lessPriority = {priority: 3, value: 'LONG'}
  pq.push(lessPriority)
  morePriorityElements.forEach(el => pq.push(el))
  t.deepEqual(pq.peek(2).value, 'LONG')
})
