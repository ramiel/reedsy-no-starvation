const cloneDeep = require('lodash/cloneDeep')

const LESS_IS_HIGH_PRIORITY = (a, b) => a - b

class PriorityQueue {
  constructor (strategy) {
    this.heap = []
    this.strategy = strategy || LESS_IS_HIGH_PRIORITY
  }

  length () {
    return this.heap.length
  }

  isEmpty () {
    return this.length() === 0
  }

  push (element) {
    this.heap.push(element)
    this._bubbleUp(this.length() - 1)
    return this
  }

  pop () {
    const [head, ...remaining] = this.heap
    this.heap = remaining
    return head
  }

  peek (i = 0) {
    return cloneDeep(this.heap[i])
  }

  _compare (el1, el2) {
    return this.strategy(el1.priority, el2.priority)
  }

  _bubbleUp (i) {
    const pi = i - 1
    if (pi >= 0 && this.heap[pi] && this._compare(this.heap[i], this.heap[pi]) < 0) {
      this.heap[pi].priority--
      [this.heap[i], this.heap[pi]] = [this.heap[pi], this.heap[i]]
      return this._bubbleUp(pi)
    }
  }
}

module.exports = PriorityQueue
