const express = require('express')
const config = require('config')

const port = config.get('server.port')
const app = express()

app.post('/job', (req, res) => {
  res.send('ciao')
})

app.listen(port, err => {
  if (err) {
    return console.err(err)
  }
  console.log(`Reedsy app listening on *:${port}`)
})
