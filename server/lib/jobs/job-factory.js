const uuid = require('uuid/v4')

const JOB_TYPE = {
  HTML: 'HTML',
  PDF: 'PDF'
}

const JOB_STATUS = {
  QUEUED: 'QUEUED',
  RUNNING: 'RUNNING',
  SUCCEDED: 'SUCCEDED',
  ERRORED: 'ERRORED'
}

const JobFactory = (data, type) => {
  // switch (type) {
  //   case JOB_TYPE.HTML:
  //   case JOB_TYPE.PDF:
  //     break
  //   default:
  //     return {
  //       id: uuid(),
  //       data,
  //       type,
  //       created: new Date(),
  //       status: JOB_STATUS.QUEUED
  //     }
  // }
  return {
    id: uuid(),
    data,
    type,
    created: new Date(),
    status: JOB_STATUS.QUEUED
  }
}

module.exports = JobFactory
