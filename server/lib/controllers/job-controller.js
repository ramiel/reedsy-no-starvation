const JobFactory = require('../creators/job-factory')

const JobController = (queue) => ({
  create: (req, res) => {
    const {data, type} = req.body
    const job = JobFactory(data, type)
    queue.push(job)
  }
})

module.exports = JobController
